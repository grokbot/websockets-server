package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
	melody "gopkg.in/olahol/melody.v1"
)

var (
	pubSub    *redis.PubSubConn
	redisConn = func() (redis.Conn, error) {
		return redis.Dial("tcp", "redis:6379")
	}
	pubSubConn = func() (redis.Conn, error) {
		return redis.Dial("tcp", "redis:6379")
	}
)

type Message struct {
	DeliveryID string `json:"id"`
	Content    string `json:"content"`
}

func main() {
	r := gin.Default()
	m := melody.New()

	redisConn, err := redisConn()
	if err != nil {
		panic(err)
	}
	defer redisConn.Close()

	go deliverMessages(m)

	r.GET("/", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "index.html")
	})

	r.GET("/ws", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})

	m.HandleMessage(func(s *melody.Session, msg []byte) {
		var message Message
		json.Unmarshal([]byte(msg), &message)
		redisConn.Do("PUBLISH", "messages", message.Content)
	})

	r.Run(":3000")
}

func deliverMessages(m *melody.Melody) {
	pubSubConn, err := redisConn()
	if err != nil {
		panic(err)
	}
	defer pubSubConn.Close()

	pubSub = &redis.PubSubConn{Conn: pubSubConn}
	defer pubSub.Close()

	if err := pubSub.Subscribe("messages"); err != nil {
		panic(err)
	}
	for {
		switch v := pubSub.Receive().(type) {
		case redis.Message:
			content := string(v.Data)
			message := Message{
				DeliveryID: v.Channel,
				Content:    content,
			}
			msg, err := json.Marshal(message)
			if err != nil {
				panic(err)
			}
			m.Broadcast(msg)
		case redis.Subscription:
			log.Printf("subscription message: %s: %s %d\n", v.Channel, v.Kind, v.Count)
		case error:
			log.Println("error pub/sub on connection, delivery has stopped")
			log.Println(v)
			return
		}
	}
}
